Awk Command Description:
Awk Was Created At Bell Labs In The 1970s And Its Name Is Derived From The Surnames Of Its Authors: Alfred Aho,Peter Weinberger, And Brian Kernigah. Awk Is a Domain-Specific Language Designed For Text Processing And Typically Used As Data Extraction And Reporting Tool.

Some Features Of Awk Command Are As Follows:

It Basically Compares The Input Text Or A Segment Of A Text File.

It Performs Various Actions On A FIle Like Searching A Specified Text And More.

It Transforms The Files And Data On A Specified Structure.

It Performs Arithmetic And String Operations.

It Applies The Conditions And Loops On Output.

AWK COMMAND SYNTAX
awk options 'selection _criteria{action }' input-file>output-file.

Awk Example
Consider A Text File To Be Processed With The Following Content-

1)Amit         Physics     80

2)Rahul      Maths       90

3)Shyam       BIology     87

Printing Column Or Field

You Can Instruct AWK To Print Only Certain Columns From The Input Field. The Following Example Demonstrates This -

Example

[jerry]$ awk '{print $3 "\t" $4}' file.txt

On Executing This Code,You Get The Following Result-

Output -

Physics       80

Maths       90

Biology       87




SED COMMAND DESCRIPTION:


Sed Was Developed From 1973 To 1974 By Lee E. McMahon Of Bell Labs And Is

Available Today For Most Operating System. Sed Is A Unix Utility

That Parses And Transforms Text, Using A Simple Programming Language.

Sed Was One Of The Earliest Tools To Support Regular Expressions.

Sed is Used For Finding, Filtering, Text SUbstitution, Replacement And

Text Manipulations Like Insertion, Deletion Search Etc.



SED COMMAND SYNTAX:

sed [options]...     [script][file]



BASIC TEXT SUBSTITUTION USING SED

Any Particular Part Of A Text Can Be Searched And Replaced By Using Searching And Replacing Pattern By Using Sed Command. In The Following Example, 's' Indicates The Search ANd Replace Task. The Word 'Bash' Will Be Searched In The Text, "Bash SCripting Language" And If The Word Exists In The Text Then It Will Be Replaced By The Word 'Pearl'.

$ echo   "Bash Scripting Language"  |  sed  's/Bash/Perl/'

OUTPUT:

The Word  'Bash' Exists In The Text. So The Output Is  'Perl SCripting Language'.


'sed' Command Can Be Used To SUbstitution Any Part Of A File Content Also.

Create A Text File Nmaed Weekday.txt With The Following Content.


Weekday.txt


Monday

Tuesday

Wednesday

Thrusday

Friday

Saturday

Sunday


The Following Command Will Search And Replace The Text 'SUnday', By The Text 'Sunday Is Holiday'.


$ cat Weekday.txt

$ sed 's/Sunday/Sunday Is Holiday/' Weekday.txt


Output:

'Sunday Exists In Weekday.txt File And This Word Is Replaced By The Text,'Sunday Is Holiday' After Executing THe ABove 'sed' Command.

Awk Command Description:
Awk Was Created At Bell Labs In The 1970s And Its Name Is Derived From The Surnames Of Its Authors: Alfred Aho,Peter Weinberger, And Brian Kernigah. Awk Is a Domain-Specific Language Designed For Text Processing And Typically Used As Data Extraction And Reporting Tool.

Some Features Of Awk Command Are As Follows:

It Basically Compares The Input Text Or A Segment Of A Text File.

It Performs Various Actions On A FIle Like Searching A Specified Text And More.

It Transforms The Files And Data On A Specified Structure.

It Performs Arithmetic And String Operations.

It Applies The Conditions And Loops On Output.

AWK COMMAND SYNTAX
awk options 'selection _criteria{action }' input-file>output-file.

Awk Example
Consider A Text File To Be Processed With The Following Content-

1)Amit         Physics     80

2)Rahul      Maths       90

3)Shyam       BIology     87

Printing Column Or Field

You Can Instruct AWK To Print Only Certain Columns From The Input Field. The Following Example Demonstrates This -

Example

[jerry]$ awk '{print $3 "\t" $4}' file.txt

On Executing This Code,You Get The Following Result-

Output -

Physics       80

Maths       90

Biology       87




SED COMMAND DESCRIPTION:
Sed Was Developed From 1973 To 1974 By Lee E. McMahon Of Bell Labs And Is

Available Today For Most Operating System. Sed Is A Unix Utility

That Parses And Transforms Text, Using A Simple Programming Language.

Sed Was One Of The Earliest Tools To Support Regular Expressions.

Sed is Used For Finding, Filtering, Text SUbstitution, Replacement And

Text Manipulations Like Insertion, Deletion Search Etc.



SED COMMAND SYNTAX:

sed [options]...     [script][file]



BASIC TEXT SUBSTITUTION USING SED

Any Particular Part Of A Text Can Be Searched And Replaced By Using Searching And Replacing Pattern By Using Sed Command. In The Following Example, 's' Indicates The Search ANd Replace Task. The Word 'Bash' Will Be Searched In The Text, "Bash SCripting Language" And If The Word Exists In The Text Then It Will Be Replaced By The Word 'Pearl'.

$ echo   "Bash Scripting Language"  |  sed  's/Bash/Perl/'

OUTPUT:

The Word  'Bash' Exists In The Text. So The Output Is  'Perl SCripting Language'.


'sed' Command Can Be Used To SUbstitution Any Part Of A File Content Also.

Create A Text File Nmaed Weekday.txt With The Following Content.


Weekday.txt


Monday

Tuesday

Wednesday

Thrusday

Friday

Saturday

Sunday


The Following Command Will Search And Replace The Text 'SUnday', By The Text 'Sunday Is Holiday'.


$ cat Weekday.txt

$ sed 's/Sunday/Sunday Is Holiday/' Weekday.txt


Output:

'Sunday Exists In Weekday.txt File And This Word Is Replaced By The Text,'Sunday Is Holiday' After Executing THe ABove 'sed' Command.


REFRENCE LINKS:

Visit Website1

Visit Website2

Visit Website3

Visit Website4

Visit Website5

